using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public GameObject weaponDrop;
    public WeaponSO[] weaponsInInventory;
    public WeaponSO selectedWeapon;
    int currentSelectionWeapon = 0;
    static Inventory inventory;
    Transform player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if(inventory != null)
        {
            Destroy(gameObject);
        }
        else
        {
            inventory = this;
        }
    }

    public static Inventory GetInventory()
    {
        return inventory;
    }

    public void SetNewWeapon(WeaponSO newWeapon)
    {
        if(weaponsInInventory[currentSelectionWeapon] == null)
        {
            selectedWeapon = weaponsInInventory[currentSelectionWeapon] = newWeapon;

        }
        else
        {
            DropWeapon(weaponsInInventory[currentSelectionWeapon]);
            selectedWeapon = weaponsInInventory[currentSelectionWeapon] = newWeapon;
        }
    }

    public void DropWeapon(WeaponSO weapon)
    {
        weaponsInInventory[currentSelectionWeapon] = null;
        
        GameObject itemWeapon = Instantiate(weaponDrop,player.position,weaponDrop.transform.rotation);
        itemWeapon.name = weapon.name;
        itemWeapon.GetComponent<SpriteRenderer>().sprite = weapon.weaponImage;

    }

    public void ChangeWeaponScroll(int selected)
    {
        currentSelectionWeapon += selected;

        if(currentSelectionWeapon < 0)
        {
            currentSelectionWeapon = 3;
        } 
        else if(currentSelectionWeapon > 3)
        {
            currentSelectionWeapon = 0;
        }
        selectedWeapon = weaponsInInventory[currentSelectionWeapon];
        selectedWeapon.bullet.GetComponent<Bullet>().damage = selectedWeapon.damage;
    }

    public void ChangeWeaponKey(int selected)
    {
        currentSelectionWeapon = selected;

        selectedWeapon = weaponsInInventory[currentSelectionWeapon];
        selectedWeapon.bullet.GetComponent<Bullet>().damage = selectedWeapon.damage;
    }
}
