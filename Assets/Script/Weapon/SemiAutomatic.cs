using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Weapons/SemiAutomatic", order = 2)]
public class SemiAutomatic : WeaponSO
{
    public int amounOfBulletToShoot;

    public override IEnumerator Shoot(Transform shootPoint)
    {
        for(int i = 0; i < amounOfBulletToShoot;i++)
        {
            Instantiate(bullet,shootPoint.position,shootPoint.rotation);
            yield return new WaitForSeconds(timeBetweenShoot);
        }
    }
}
