using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Weapons/Automatic", order = 2)]
public class Automatic : WeaponSO
{
    public float maxDispersion;
    float saveDispersion;

    public override IEnumerator Shoot(Transform shootPoint)
    {
        saveDispersion = dispersion;
        Quaternion weaponRotation = shootPoint.rotation;
        Quaternion direction = shootPoint.rotation;
        while(true)
        {
            float dispersionAngle = Random.Range(-dispersion,dispersion);
            direction = weaponRotation * Quaternion.AngleAxis(dispersionAngle, Vector3.up);
            weaponRotation = shootPoint.rotation;
            
            Instantiate(bullet,shootPoint.position,direction);
            yield return new WaitForSeconds(timeBetweenShoot);

            if(!Input.GetMouseButton(0)) 
            {
                break;
            }

            if(dispersion < maxDispersion)
            {
                dispersion++;
            }

            
        }

        dispersion = saveDispersion;
    }
}
