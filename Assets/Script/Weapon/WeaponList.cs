using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponList : MonoBehaviour
{
    public WeaponSO[] list;
    static WeaponList weaponList;

    void Awake()
    {
        if(weaponList != null)
        {
            Destroy(gameObject);
        }
        else
        {
            weaponList = this;
        }
    }

    public static WeaponList GetList()
    {
        return weaponList;
    }

    public WeaponSO GetWeaponInList(string name)
    {
        foreach(WeaponSO weapon in list)
        {
            if(weapon.name == name)
            {
                return weapon;
            }
        }
        return null;
    }
}
