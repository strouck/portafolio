using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Weapons/Shotgun", order = 2)]
public class Shotgun : WeaponSO
{
    public int amounOfBulletToShoot;

    public override IEnumerator Shoot(Transform shootPoint)
    {
        for(int i = 0; i < amounOfBulletToShoot;i++)
        {
            Quaternion weaponRotation = shootPoint.rotation;

            float dispersionAngle = Random.Range(-dispersion,dispersion);
            Quaternion direction = weaponRotation * Quaternion.AngleAxis(dispersionAngle, Vector3.up);

            Instantiate(bullet,shootPoint.position,direction);
        }
        yield return new WaitForSeconds(timeBetweenShoot);
    }
}
