using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Weapons/Default", order = 1)]
public class WeaponSO : ScriptableObject
{
    [SerializeField]protected float timeBetweenShoot;
    [SerializeField]public int damage;
    public Sprite weaponImage;
    [SerializeField]public GameObject bullet;
    public float dispersion;

    public virtual IEnumerator Shoot(Transform shootPoint)
    {
        Instantiate(bullet,shootPoint.position,shootPoint.rotation);
        yield return new WaitForSeconds(timeBetweenShoot);
    }
}
