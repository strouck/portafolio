using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamDirection : MonoBehaviour
{
    static public Vector3 fordwardCam;
    static public Vector3 rightCam;

    void Start()
    {
        SetDirectionCam();
    }

    public void SetDirectionCam()
    {
        fordwardCam = transform.up;
        rightCam = transform.right;

        fordwardCam.y = 0;
        rightCam.y = 0;

        fordwardCam = fordwardCam.normalized;
        rightCam = rightCam.normalized;
    }
}
