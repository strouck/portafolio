using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{
    public float velocityOfRotate;

    public void Rotate(Vector3 direction)
    {
        direction.y = 0;

        Quaternion directionToRotate = Quaternion.LookRotation(direction) ;
        transform.rotation = Quaternion.Lerp(transform.rotation,directionToRotate,velocityOfRotate);
    }

    public Vector3 GetRotateOfMouseDirection()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out RaycastHit info)){
        
            return info.point - transform.position;
        }
        else
        {

            Vector3 mouseDirection = ray.origin + ray.direction * (ray.origin-transform.position).magnitude; 
            return mouseDirection - transform.position;
        }
    }
}
