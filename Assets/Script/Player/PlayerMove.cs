using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] float velocity;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Movement(Vector3 direction)
    {
        direction = direction.x * CamDirection.rightCam + direction.z * CamDirection.fordwardCam ;
        rb.velocity = direction.normalized * velocity; 
    }
}
