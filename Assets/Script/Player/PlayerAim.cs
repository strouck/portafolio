using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.Device;

public class PlayerAim : MonoBehaviour
{
    public GameObject crosshair;
    public bool checkAim;

    public void EnabledCrosshair()
    {
        crosshair.transform.position = Input.mousePosition;
    }
}
