using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Action OnAim;
    public Action OnExitAim;
    public Action OnShoot;
    public Action OnExitShoot;
    bool aim;

    public void ScrollMouse()
    {
        if(Input.GetAxis("Mouse ScrollWheel") != 0) 
        {
            Inventory.GetInventory().ChangeWeaponScroll((int)Input.GetAxis("Mouse ScrollWheel"));
        }
    }

    public void ChangeWeaponInput()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            Inventory.GetInventory().ChangeWeaponKey(0);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            Inventory.GetInventory().ChangeWeaponKey(1);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            Inventory.GetInventory().ChangeWeaponKey(2);
        }
    }

    public void AimInput()
    {
        if(Input.GetMouseButtonDown(1))
        {
            OnAim?.Invoke();
            aim = true;
        }

        if(Input.GetMouseButtonDown(0) && aim)
        {
            OnShoot?.Invoke();
        }

        if(Input.GetMouseButtonUp(1))
        {
            aim = false;
            OnExitAim?.Invoke();
            OnExitShoot?.Invoke();
        }

        if(Input.GetMouseButtonUp(0))
        {
            OnExitShoot?.Invoke();
        }
    }


    public Vector3 MoveInput()
    {
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"),0,Input.GetAxisRaw("Vertical"));
        return input.normalized;
    }
}
