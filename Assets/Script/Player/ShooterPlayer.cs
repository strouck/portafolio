using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterPlayer : MonoBehaviour
{
    [SerializeField]public bool canShoot = true;

    public void Shoot()
    {
        if(canShoot == true && Inventory.GetInventory().selectedWeapon != null)
        {
            StartCoroutine(ShootWeapon());
        }
    }

    IEnumerator ShootWeapon()
    {
        canShoot = false;
        yield return Inventory.GetInventory().selectedWeapon.Shoot(transform);
        canShoot = true;
    }
}
