using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float velocity;
    [SerializeField] float lifeTime;
    Rigidbody rb;
    public int damage;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject,lifeTime);
        rb.velocity = transform.forward * velocity;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != "Bullet" && other.gameObject.tag != "Item")
        {
            Destroy(gameObject,1);
            rb.velocity = Vector3.zero;
            if(other.GetComponent<IDamageable>() != null)
            {
                other.GetComponent<IDamageable>().TakeDamage(damage);
            }
        }
        
    }
}
