using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    RotatePlayer rotate;
    [SerializeField] PlayerAim aim;
    [SerializeField] PlayerMove playerMove;
    [SerializeField] PlayerInput input;
    [SerializeField] ShooterPlayer shooter;

    void Start()
    {
        
        aim = GetComponent<PlayerAim>();
        playerMove = GetComponent<PlayerMove>();
        input = GetComponent<PlayerInput>();
        rotate = GetComponent<RotatePlayer>();

        input.OnShoot += () => Shoot();
        input.OnAim += () => EnabledCrosshair();
        input.OnExitAim += () => DisabledCrosshair();
        
    }
    
    void Update()
    {
        Movement();
        input.AimInput();
        input.ScrollMouse();
        input.ChangeWeaponInput();
    }

    void Shoot()
    {
        shooter.Shoot();
    }

    public void Movement()
    {
        playerMove.Movement(input.MoveInput());
        if(!aim.checkAim && input.MoveInput() != Vector3.zero)
        {
            rotate.Rotate(input.MoveInput());
        }
    }

    public void EnabledCrosshair()
    {
        //ejecutarAnimacion
        aim.checkAim = true;
        aim.crosshair.SetActive(true);
        StartCoroutine(Pointing());
    }

    void DisabledCrosshair()
    {
        aim.crosshair.SetActive(false);
        aim.checkAim = false;
    }

    IEnumerator Pointing()
    {
        while(aim.checkAim)
        {
            aim.EnabledCrosshair();

            rotate.Rotate(rotate.GetRotateOfMouseDirection());
            yield return null;
        }
    }
}
