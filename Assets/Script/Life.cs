using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour,IDamageable
{
    public int maxLife,currentLife;

    void Start()
    {
        currentLife = maxLife;
    }

    public void TakeDamage(int damage)
    {
        currentLife -= damage;
    }
}
