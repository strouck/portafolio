using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemInteractable : MonoBehaviour
{
    protected bool canInteract;

    public virtual IEnumerator InteractItem()
    {
        canInteract = true;
        while(canInteract)
        {
            if(Input.GetKeyDown(KeyCode.E))
            {
                ExecuteInteract();
                Destroy(gameObject);
                canInteract = false;
            }
            yield return null;
        }
    }

    public abstract void ExecuteInteract();
}
