using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemWeapon : ItemInteractable
{
    public override void ExecuteInteract()
    {
        Inventory.GetInventory().SetNewWeapon(WeaponList.GetList().GetWeaponInList(gameObject.name));
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            StartCoroutine(InteractItem());
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            canInteract = false;
        }
    }
}
